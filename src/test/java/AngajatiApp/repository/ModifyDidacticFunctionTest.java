package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.controller.EmployeeController;
import AngajatiApp.model.Employee;
import mkci0201MV.AppTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ModifyDidacticFunctionTest extends AppTest {

    EmployeeController employeeController;

    @BeforeEach
    void setup() {

        EmployeeMock employeeMock = new EmployeeMock();
        employeeController = new EmployeeController(employeeMock);
    }

    @Test
    void modifyEmployeeFunctionWithEmployeeAsNull_TC01(){

        List<Employee> employeeList = employeeController.getEmployeesList();

        Employee newEmployee = null;
        employeeController.modifyEmployee(newEmployee, DidacticFunction.CONFERENTIAR);
        assertEquals(employeeList,employeeController.getEmployeesList());

    }

    @Test
    void modifyEmployeeFunctionWithData_Tc02(){

        Employee oldEmployee = employeeController.findEmployeeById(5);
        employeeController.modifyEmployee(oldEmployee, DidacticFunction.CONFERENTIAR);

        assertEquals(oldEmployee, employeeController.findEmployeeById(5));
    }
}