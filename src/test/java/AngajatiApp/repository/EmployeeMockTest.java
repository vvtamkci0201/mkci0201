package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.controller.EmployeeController;
import AngajatiApp.model.Employee;
import mkci0201MV.AppTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest extends AppTest {

    EmployeeController employeeController;

    @BeforeEach
    void setup() {

     EmployeeMock employeeMock = new EmployeeMock();
     employeeController = new EmployeeController(employeeMock);

    }

    // valid employee input - should add employee and increase the size of the list(repository)
    @Test
    void addEmployeeTC1_BB() {
        int currentEmployeesNumber  = employeeController.getEmployeesList().size();
        Employee testEmployee = new Employee("Maria","Marin","2901024260019",DidacticFunction.LECTURER,1200.0);

        boolean addResult = employeeController.addEmployee(testEmployee);
        assertTrue(addResult);
        assertEquals(currentEmployeesNumber+1, employeeController.getEmployeesList().size());

    }

    //invalid employee input - should not add employee to the repository (to short LastName) and return boolean false
    @Test
    void addEmployeeTC2_BB(){


        int currentEmployeesNumber  = employeeController.getEmployeesList().size();
        Employee testEmployeeTc2 = new Employee("Maria","M","2901024260019",DidacticFunction.LECTURER,1200.0);

        boolean addResult = employeeController.addEmployee(testEmployeeTc2);
        assertFalse(addResult);
        assertEquals(currentEmployeesNumber, employeeController.getEmployeesList().size());

    }

    //invalid employee input - should not add employee to repository (to short lastName) and return boolean false
    @Test
    void addEmployeeTC3_BB(){

        int currentEmployeesNumber  = employeeController.getEmployeesList().size();
        Employee testEmployeeTc3 = new Employee("Maria","Ma","2901024260019",DidacticFunction.LECTURER,1200.0);

        boolean addResult = employeeController.addEmployee(testEmployeeTc3);
        assertFalse(addResult);
        assertEquals(currentEmployeesNumber, employeeController.getEmployeesList().size());

    }

    //invalid employee input - should not add employee to repository (incorrect characters in the lastName) and return false
    @Test
    void addEmployeeTC4_BB(){

        int currentEmployeesNumber  = employeeController.getEmployeesList().size();
        Employee testEmployeeTc4 = new Employee("Maria","Ma1","2901024260019",DidacticFunction.LECTURER,1200.0);

        boolean addResult = employeeController.addEmployee(testEmployeeTc4);
        assertFalse(addResult);
        assertEquals(currentEmployeesNumber, employeeController.getEmployeesList().size());

    }

    //invalid employee input - should not add employee to repository (incorrect characters in the lastName) and return boolean false
    @Test
    void addEmployeeTC5_BB(){

        int currentEmployeesNumber  = employeeController.getEmployeesList().size();
        Employee testEmployeeTc5 = new Employee("Maria","111","2901024260019",DidacticFunction.LECTURER,1200.0);

        boolean addResult = employeeController.addEmployee(testEmployeeTc5);
        assertFalse(addResult);
        assertEquals(currentEmployeesNumber, employeeController.getEmployeesList().size());
    }

    //invalid employee input - data should not be add employee to list (lastName is null) and return boolean false

    @Test
    void addEmployeeTC6_BB(){

        int currentEmployeesNumber  = employeeController.getEmployeesList().size();
        Employee testEmployeeTc6 = new Employee("Maria","","2901024260019",DidacticFunction.LECTURER,1200.0);

        boolean addResults = employeeController.addEmployee(testEmployeeTc6);
        assertFalse(addResults);
        assertEquals(currentEmployeesNumber, employeeController.getEmployeesList().size());

    }

    //invalid employee input - should not add employee to list (salary is 0) and return boolean false
    @Test
    void addEmployeeTC7_BB(){

        int currentEmployeesNumber  = employeeController.getEmployeesList().size();
        Employee testEmployeeTc7 = new Employee("Maria","Marin","2901024260019",DidacticFunction.LECTURER,0.0);

        boolean addResult = employeeController.addEmployee(testEmployeeTc7);
        assertFalse(addResult);
        assertEquals(currentEmployeesNumber, employeeController.getEmployeesList().size());
    }

    //invalid employee input - should not add employee to the repository (no salary defined) and return boolean false
    @Test
    void addEmployeeTC8_BB(){

        int currentEmployeesNumber  = employeeController.getEmployeesList().size();
        Employee testEmployeeTc8 = new Employee();

        testEmployeeTc8.setFirstName("Maria");
        testEmployeeTc8.setLastName("Marin");
        testEmployeeTc8.setCnp("2901024260019");
        testEmployeeTc8.setSalary(-100.0);
        testEmployeeTc8.setFunction(DidacticFunction.LECTURER);

        boolean addResult = employeeController.addEmployee(testEmployeeTc8);
        assertFalse(addResult);
        assertEquals(currentEmployeesNumber, employeeController.getEmployeesList().size());
    }

    //valid employee input - employee should be added to the repository and return boolean true
    @Test
    void addEmployeeTC9_BB(){

        int currentEmployeesNumber  = employeeController.getEmployeesList().size();
        Employee testEmployeeTc9 = new Employee("Maria","Mar","2901024260019",DidacticFunction.LECTURER,1200.0);

        boolean addResult = employeeController.addEmployee(testEmployeeTc9);
        assertTrue(addResult);
        assertEquals(currentEmployeesNumber+1, employeeController.getEmployeesList().size());
    }

    //valid employee input - should be added to the repository and return boolean true
    @Test
    void addEmployeeTC10_BB(){

        int currentEmployeesNumber  = employeeController.getEmployeesList().size();
        Employee testEmployeeTc10 = new Employee("Maria","mar","2901024260019",DidacticFunction.LECTURER,1200.0);

        boolean addResult = employeeController.addEmployee(testEmployeeTc10);
        assertTrue(addResult);
        assertEquals(currentEmployeesNumber+1, employeeController.getEmployeesList().size());
    }

    //invalid employee input - employee should not be added to repository and should return boolean false
    @Test
    void addEmployeeTC11_BB(){

        int currentEmployeesNumber  = employeeController.getEmployeesList().size();
        Employee testEmployeeTc11 = new Employee("Maria","ma1","2901024260019",DidacticFunction.LECTURER,1200.0);

        boolean addResult = employeeController.addEmployee(testEmployeeTc11);
        assertFalse(addResult);
        assertEquals(currentEmployeesNumber, employeeController.getEmployeesList().size());
    }

    //valid employee input - employee should be added to the repository and return boolean true
    @Test
    void addEmployeeTC12_BB(){

        int currentEmployeesNumber  = employeeController.getEmployeesList().size();
        Employee testEmployeeTc12 = new Employee("Maria","Marin","2901024260019",DidacticFunction.LECTURER,1.0);

        boolean addResult = employeeController.addEmployee(testEmployeeTc12);
        assertTrue(addResult);
        assertEquals(currentEmployeesNumber+1, employeeController.getEmployeesList().size());
    }

    //valid employee input - employee should be added to the repository and return true
    @Test
    void addEmployeeTC13_BB(){

        int currentEmployeesNumber  = employeeController.getEmployeesList().size();
        Employee testEmployeeTc13 = new Employee("Maria","Marin","2901024260019",DidacticFunction.LECTURER,2.0);

        boolean addResult = employeeController.addEmployee(testEmployeeTc13);
        assertTrue(addResult);
        assertEquals(currentEmployeesNumber+1, employeeController.getEmployeesList().size());
    }


}