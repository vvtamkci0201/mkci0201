package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.validator.EmployeeException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EmployeeTest {

    private Employee employee;

    @BeforeEach
    public void setUp() {
        employee = new Employee("Maria", "Dumitru", "2800610260010", DidacticFunction.TEACHER, 1800.0);

    }

    @Test
    @Order(1)
    public void CreateEmployee_defaultConstructorIsNotNull() {

        Employee employeeOne = new Employee();
        assert employeeOne != null;
    }

    @Test
    @Order(2)
    public void CreateEmployee_regularConstructorNotNull() {
        Employee employeeReg = new Employee("Ion", "Dumitru", "180101220260010", DidacticFunction.CONFERENTIAR, 2800.0);
        assert employeeReg != null;
    }

    @Test
    @Order(6)
    public void CheckEmployeeName_isEqualWithInitalCreation() {
        assertEquals("Maria", employee.getFirstName());
        assertEquals("Dumitru", employee.getLastName());
    }

    @Test
    @Order(7)
    public void EmployeeCNP_isReturnedAndChanged() {
        assertEquals("2800610260010", employee.getCnp());
        employee.setCnp("1701109260011");
        assertEquals("1701109260011", employee.getCnp());
    }

    @Test
    @Order(8)
    public void EmployeeSalary_canBeReturnedAndChanged() {
        assertEquals(1800.0, employee.getSalary());
        employee.setSalary(2500.0);
        assertEquals(2500.0, employee.getSalary());
    }

    @Test
    @Order(3)
    public void EmployeePrintOut_isSeparated_withSemicolon (){
        assertEquals("Maria;Dumitru;2800610260010;TEACHER;1800.0;0", employee.toString());
    }

    @Test
    @Order(5)
    public void EmployeeException_thrownForIncorrectLength()  {
        String employeeData = "Maria;Muntean;";
        int line = 1;
       assertThrows(EmployeeException.class, ()->{Employee.getEmployeeFromString(employeeData, line);});
    }

    @Test
    @Order(4)
    public void TimeOut_notExceeded() {
        String employeeData = "Maria;Muntea;2801016300010;ASISTENT;2000.0;1;";
        int line = 1;

        assertTimeout(Duration.ofMillis(1), ()-> {
            Employee.getEmployeeFromString(employeeData, line);
        });
    }

    @ParameterizedTest
    @Order(9)
    @ValueSource(doubles ={ 2000.0, 1800.8, 2500.8 } )
    public void EmployeeSalary_Replaced(double salaryArg) {
        employee.setSalary(salaryArg);
        assertEquals(salaryArg, employee.getSalary());
    }

    @AfterEach
    public void tearDown() {

    }
}